const tabs = document.querySelectorAll('.tab');
const tabPanes = document.querySelectorAll('.tab-pane');

function clickTab(e){
    tabs.forEach(tab => tab.classList.remove('active'));
    e.target.classList.add('active');
    const targetPane = document.querySelector(e.target.dataset.target);
    
    tabPanes.forEach(tabPane => tabPane.classList.remove('active'));
    targetPane.classList.add('active');
}

tabs.forEach(tab => addEventListener('click', clickTab));